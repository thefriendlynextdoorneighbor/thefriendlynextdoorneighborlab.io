# My Portfolio

This project stores all the data for my project portfolio on my GitLab page. If you would like to see what kind of stuff I work on, feel free to click [here](https://thefriendlynextdoorneighbor.github.io/)